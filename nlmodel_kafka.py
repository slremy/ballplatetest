# python nlcontroller.py kafka internet dsci002.palmetto.clemson.edu 6667 local_new 3600 0.20 6.09 3.5 -5.18 -12.08 6.58 -0.4
# ip addr for broker = 10.1125.8.197
# sudo tcpdump -i wlan0 -n net 10.125.8.197 -w capture.cap


from nlmodel import *
from pykafka import KafkaClient
from pykafka.common import OffsetType
import logging

#Take arguments to determine file name, port, etc.
try:
    host = argv[1]
    port = int(argv[2])
    topics = argv[3]
    control_action,plant_state = [t(s) for t,s in zip((str,str),topics.split("__"))]
except:
    print(exc_info())
    print("Usage: python nlmodel_kafka.py host port controlaction__plantstate")
    print("e.g. python nlmodel_kafka.py t3.cs.clemson.edu 9898 control_action__plant_state")
    exit(1)

kafka,producer,consumer = None,None,None
control_group = "control_group"

def initialize_handshake(HOST, PORT):    # setup socket and start the connection to the model
    global kafka,producer,consumer
    timeout = 150
    server = HOST+":"+str(PORT)

    kafka = KafkaClient(hosts=server)
    
    producer_topic = kafka.topics[plant_state]
    consumer_topic = kafka.topics[control_action]

    producer = producer_topic.get_producer(min_queued_messages=1, max_queued_messages=1)

    consumer = consumer_topic.get_simple_consumer(consumer_group=control_group, auto_offset_reset=OffsetType.LATEST, reset_offset_on_start=True)
    print "Model's Consumer and SimpleProducer successfully connected and now going to send opening message..."
    producer.produce(interpret("/state?&]"))


  #  read from the consumer

def read_from_consumer():
    global controlreq, old_controlreq #, consumer, no_produce
    try:
        controlreq = consumer.consume(block=False).value # block=False ?
        producer.produce(interpret(controlreq)) #assumes that the request isn't a duplicate
    except:
        pass;


if __name__ == '__main__':
	initialize_handshake(host, port)

	signal.signal(signal.SIGALRM, updateState)
	signal.setitimer(signal.ITIMER_REAL, h, h)
    
	notFinished = True
	#wait until you can't listen anymore
	while notFinished:
		try:
			read_from_consumer()
			sleep(.01)
		except (KeyboardInterrupt, SystemExit):
			notFinished = False
			signal.setitimer(signal.ITIMER_REAL, 0, h)
			print(exc_info())
			print("Shutting down service")